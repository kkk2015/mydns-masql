-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        10.1.16-MariaDB-1~xenial - mariadb.org binary distribution
-- 服务器操作系统:                      debian-linux-gnu
-- HeidiSQL 版本:                  9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 dnsmasq 的数据库结构
CREATE DATABASE IF NOT EXISTS `dnsmasq` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dnsmasq`;


-- 导出  表 dnsmasq.domain 结构
CREATE TABLE IF NOT EXISTS `domain` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `domain` varchar(125) NOT NULL DEFAULT '0',
  `ip` varchar(15) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '解析状态：0启用，1禁用',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='域名表';

-- 正在导出表  dnsmasq.domain 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `domain` DISABLE KEYS */;
INSERT INTO `domain` (`id`, `domain`, `ip`, `status`, `create_time`, `update_time`) VALUES
	(8, 'srs.kkk.com', '10.10.100.21', 0, '2016-10-23 16:39:07', NULL),
	(9, 'www.kkk.com', '10.10.100.8', 0, '2016-10-23 16:51:00', NULL),
	(10, 'test.kkk.com', '10.10.100.200', 0, '2016-10-23 16:52:41', NULL),
	(11, 'test.qq.com', '10.10.100.254', 0, '2016-10-23 16:56:12', NULL);
/*!40000 ALTER TABLE `domain` ENABLE KEYS */;


-- 导出  表 dnsmasq.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `passwd` varchar(32) NOT NULL COMMENT '密码',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态：0正常，1禁用',
  `is_acive` int(1) NOT NULL DEFAULT '0' COMMENT '0没有激活，1激活',
  `qx` int(1) NOT NULL DEFAULT '0' COMMENT '权限：0普通，1超级',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- 正在导出表  dnsmasq.user 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
