#encoding:utf-8
'''
jk409@qq.com created by 2015-6-6
'''
import json
import requests
import urllib,urllib2

def get(url):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              }
    res=requests.get(url,headers=header)
    #print res.content,res.status_code
    #"success:201"
    return res



def post(url,data):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0"
              }
    data=json.JSONEncoder().encode(data)
    res=requests.post(url,data,headers=header)
    return res

def delete(url):
    res=requests.delete(url)
    return res

#====================================================================================================
'''
cafile = 'cacert.pem' # http://curl.haxx.se/ca/cacert.pem
r = requests.get(url, verify=cafile)
'''
def tlogin(url,username,password):
    header = {
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              "Content-Type": "application/json",
              "username":"%s"%username,
              "password":"%s"%password
              }
    res = requests.get(url, headers=header,verify=False)
    # print res.content,res.status_code
    # "success:201"
    return json.JSONDecoder().decode(res.text)

def flush_token(url,token):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              "token":token
              }
    #url='http://%s/api/pmt-jfpayment-1.3-SNAPSHOT/v3/auth/refresh?refresh_token=%s&apiKey=%s'%(ip,refresh_token,client_id)
    res = requests.get(url, headers=header,verify=False)
    return res

def tget(url,token):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              "Authorization":"Bearer %s"%token
              }
    res = requests.get(url, headers=header,verify=False)
    # print res.content,res.status_code
    # "success:201"
    return json.JSONDecoder().decode(res.text)

def tpost(url,data,token):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              "Authorization": "Bearer %s" % token
              }
    data=json.JSONEncoder().encode(data)
    res=requests.post(url,data,headers=header,verify=False)
    return res

def tdelete(url,token):
    header = {"Content-Type": "application/json",
              "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
              "Authorization": "Bearer %s" % token
              }
    #data=json.JSONEncoder().encode(data)
    res=requests.delete(url,headers=header,verify=False)
    return res

if __name__ == "__main__":
    token='eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiIxMzE2NjY2ODg4OCIsIm5iZiI6MTQ3MDAzNzYzMCwiaXNzIjoic3otYWdhdGhhLmNvbSIsImV4cCI6MTQ3MDAzODgzMCwiaWF0IjoxNDcwMDM3NjMwfQ.L4UomYlMMS1M6_zfO-rIa5qurfSXU8vrIguepVDQUrRI3pRbcj3NYxV0aGbVV0N2k93iAmxAGDJhKtP4t7hwmrFT6z7NExLaypSTJFlMU9Vefk8snCzZx-XNHJX7tjK3GGcXJy2q-UzwhvTDKVYKUXO2HROJvexjfRtf49fA1XeXvXTAfJpx_hs_YmxZF5AH99vi_IiKXWTF-7yH4A91cE-DaL4IC-FXz3AmBf6BhpFy6Ac7oG-BTO6BC0OP0uV5P1Lzyttw3uCfHw4J5wygt6axM2wcLiRuqGchiaeCLOmJPBwQs1o-yAVK28gOIXT2EvG-14JYl5MXrLZ5xcQEKg'
    res=tget('https://%s/pmt-jfpayment-1.3-SNAPSHOT/staff/12?apiKey=1&branchNo=1',token)
    #print type(res.text),res.text
    res=json.JSONDecoder().decode(res.text)
    print res
    pass