__author__ = 'Administrator'
#E-mail:jk409@qq.com
#example: test.py   ip   80
import sys,time,socket
from multiprocessing import Process, Pool, Lock

def check_port(ip,port):
    #time.sleep(0.8)
    try:
        time.sleep(0.01)
        sc=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        sc.settimeout(2)
        sc.connect((ip,int(port)))
        return [ip,port,'success']
    #except socket.timeout:
    #    return [ip,port,'danger']
    except:
        return [ip,port,'danger']



def run(Iplist):
    datas=[]
    result = []
    #----------------------------------------
    ProcessPools = Pool(processes=2)
    for i in Iplist:
        result.append(ProcessPools.apply_async(check_port, ('%s'%i[0], '%s'%i[1],)))
    ProcessPools.close()
    #---------------------------------------
    for res in result:
        datas.append(res.get())
    return datas

if __name__ =="__main__":
    iplist=[
        ['127.0.0.1',8080],
            ]
    print(run(iplist))


